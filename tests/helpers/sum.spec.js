const chai = require("chai");
const expect = chai.expect;
const sum = require("../../helpers/sum");

describe("smoke test", function() {
  it("checks equality", function() {
    expect(sum(1, 2)).to.be.equal(3);
  });
});
